// ==UserScript==
// @name     #GreensharpOnSNOW
// @author   Leonardo Ferraguzzi
// @version  0.0.828
// @description Add Greensharp on call persons on SNOW activities report
// @include  https://whirlpool.service-now.com/sys_report_display.do?sysparm_report_id=311edd4c4f33d78893b6fb95f110c73e
// @require  http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js
// @require  https://raw.githubusercontent.com/moment/moment/develop/moment.js
// @resource resCal https://gitlab.com/lferra/GreensharpOnSNOW/raw/master/calendar.xml
// @updateURL https://gitlab.com/lferra/GreensharpOnSNOW/raw/master/GreensharpOnSNOW.user.js
// @downloadURL https://gitlab.com/lferra/GreensharpOnSNOW/raw/master/GreensharpOnSNOW.user.js
// @grant    GM_getResourceText
// ==/UserScript==
(function() {
    'use strict';

    var gmCalendar = GM_getResourceText("resCal");
    var timeFormat = "HH:mm:ss";
    var dateFormat = "DD-MM-YYYY";
    var taskDateFormat = "DD-MM-YYYY HH:mm:ss"

    var parser = new DOMParser();
    var xmlDoc = parser.parseFromString(gmCalendar, "text/xml");

    var cellContentOnCall = '<th name="oncall" class="list_hdrcell_nosort " title="" align="left" scope="col">G# on call</th>';
    var cellContentOnCallNext = '<th name="oncall_next" class="list_hdrcell_nosort " title="" align="left" scope="col">G# next on call</th>';
    $( cellContentOnCall ).insertAfter( $( "[name='short_description']" ) );
    $( cellContentOnCallNext ).insertAfter( $( "[name='oncall']" ) );

    // ^ begins with
    // https://stackoverflow.com/a/3496338 to map text object inside an array
    var dateArray = $( "[class^='list_row']" )
    .contents().filter( "td:nth-child(6)" )
    .map(function(){
        return $(this).text();
    }).get();

    var arrayLength = dateArray.length;
    for (var i = 0; i < arrayLength; i++)
    {
        // extract date and time
        var taskMoment = moment(dateArray[i], taskDateFormat );
        var date = taskMoment.format(dateFormat);
        var time = taskMoment.format(timeFormat);
        var periodOnCall = null;
        var periodNextOncall = null;

        var momentBeginAM = moment(date + " 09:00:00", taskDateFormat);
        var momentBeginPM = moment(date + " 18:00:00", taskDateFormat);
        var momentMidnight = moment(date + " 00:00:00", taskDateFormat);

        // compute period of OnCall person
        if ( taskMoment.isBetween(momentMidnight, momentBeginAM, null, '[)') )
        {
            taskMoment.subtract(1, 'days');
            periodOnCall = "PM";
        }
        else
        {
            if ( taskMoment.isBetween(momentBeginAM, momentBeginPM, null, '[)') )
            {
                periodOnCall = "AM";
            }
            else
            {
                periodOnCall = "PM";
            }
        }

        // Build xpath for OnCall person query
        var xpathOnCall = "/GM_Calendar/GM_Year[@value='" +
            taskMoment.format("YYYY") + "']/GM_Month[@value='" +
            taskMoment.format("MM") + "']/GM_Day[@value='" +
            taskMoment.format("DD") + "']/" + periodOnCall;

        // compute period of next OnCall person
        if(periodOnCall == "AM")
        {
            periodNextOncall = "PM"
        }
        else
        {
            taskMoment.add(1, 'days');
            periodNextOncall = "AM";
        }

        // Build xpath for next OnCall person query
        var xpathNextOnCall = "/GM_Calendar/GM_Year[@value='" +
            taskMoment.format("YYYY") + "']/GM_Month[@value='" +
            taskMoment.format("MM") + "']/GM_Day[@value='" +
            taskMoment.format("DD") + "']/" + periodNextOncall;

        var onCall = getElementByXpath(xmlDoc, xpathOnCall).textContent;
        var nextOnCall = getElementByXpath(xmlDoc, xpathNextOnCall).textContent;

        var rowContentOnCall = '<td class="vt" style=";"><span style="">' + onCall + '</span></td>'
        var rowContentNextOnCall = '<td class="vt" style=";"><span style="">' + nextOnCall + '</span></td>'

        $( "[class^='list_row']:eq(" + i + ")" ).contents().filter("[class='vt vt-spacer']").before(rowContentOnCall).before(rowContentNextOnCall);
    }

function getElementByXpath(document, path) {
  return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

})();
